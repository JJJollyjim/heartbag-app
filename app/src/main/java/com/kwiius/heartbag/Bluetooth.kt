package com.kwiius.heartbag

import android.util.Log
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.RxBleConnection
import com.polidea.rxandroidble2.Timeout
import io.reactivex.disposables.Disposable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.ReplaySubject
import java.util.*
import java.util.concurrent.TimeUnit

class Bluetooth(val rxBleClient: RxBleClient) {
    val macAddress = "5D:2E:CF:F9:1E:3F"
    val device = rxBleClient.getBleDevice(macAddress)
    val TAG = "BluetoothStuff"
    val connState: ReplaySubject<Boolean> = ReplaySubject.create()

    var conn: RxBleConnection? = null
    init {
        connState.onNext(false)

        device.observeConnectionStateChanges()
                .subscribe(
                        { state ->
                            Log.i(TAG, "agowerjogui")
                            when (state) {

                                RxBleConnection.RxBleConnectionState.CONNECTED -> connState.onNext(true)
                                RxBleConnection.RxBleConnectionState.DISCONNECTED -> {
                                    // Reconnect
                                    Log.i(TAG, "Disconnected, reconnect")
                                    connState.onNext(false)
                                }
                            }
                        },
                        { throwable -> }
                )

        device.establishConnection(false, Timeout(10, TimeUnit.SECONDS))
                .subscribe(
                        { rxBleConnection ->
                            this.conn = rxBleConnection
                            var state: Byte = 1
//                            Observable.interval(100, TimeUnit.MILLISECONDS)
//                                    .observeOn(AndroidSchedulers.mainThread())
//                                    .subscribeOn(AndroidSchedulers.mainThread())
//                                    .subscribe {
//                                        val stateToSet = state
//                                        rxBleConnection.writeCharacteristic(UUID.fromString("0000fb23-3079-ddf1-f6a6-d60865a05740"), byteArrayOf(stateToSet)).subscribe(
//                                                { x -> Log.i(TAG, "${x[0] == stateToSet}" )},
//                                                { t -> throw t }
//                                        )
//
//                                        state = if (state == (0.toByte())) 1 else 0
//
//                                    }
                        },
                        { throwable->
                            throw throwable
                        })
    }

    fun setOn(on: Boolean): Single<ByteArray> {
        return conn!!.writeCharacteristic(UUID.fromString("0000fb23-3079-ddf1-f6a6-d60865a05740"), byteArrayOf(if (on) 1 else 0))!!
    }

    fun setBrightness(brightness: Int): Single<ByteArray> {
        return conn!!.writeCharacteristic(UUID.fromString("0000d67d-1f06-9fe1-6944-f87ac4d8ab73"), byteArrayOf((brightness shl 8).toByte(), brightness.toByte()))
    }
}


