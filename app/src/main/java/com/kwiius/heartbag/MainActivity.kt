package com.kwiius.heartbag

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.View
import android.view.ViewManager
import android.widget.ArrayAdapter
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.changes
import com.jakewharton.rxbinding2.widget.checkedChanges
import com.jakewharton.rxbinding2.widget.itemSelections
import com.madrapps.pikolo.HSLColorPicker
import com.madrapps.pikolo.listeners.SimpleColorSelectionListener
import com.polidea.rxandroidble2.RxBleClient
import io.reactivex.Observable
import org.jetbrains.anko.contentView
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.dip
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.padding
import org.jetbrains.anko.sdk25.coroutines.onCheckedChange
import org.jetbrains.anko.sdk25.coroutines.onItemSelectedListener
import org.jetbrains.anko.sdk25.coroutines.onSeekBarChangeListener
import org.jetbrains.anko.seekBar
import org.jetbrains.anko.spinner
import org.jetbrains.anko.toggleButton
import org.jetbrains.anko.verticalLayout

inline fun ViewManager.hslColorPicker(init: HSLColorPicker.() -> Unit): HSLColorPicker {
    return ankoView({ HSLColorPicker(it) }, theme = 0, init = init)
}


class MainActivity : Activity() {
    val TAG = "beans"
    var gatt: BluetoothGatt? = null
    val buttons = mutableSetOf<View>()
    var rxBleClient: RxBleClient? = null
    var snackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rxBleClient = RxBleClient.create(this)

        if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            this.requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), 1)
        }

        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        val bluetoothAdapter = bluetoothManager.adapter

        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, 1)
        } else {
//            connect();
        }

        val bt = Bluetooth(rxBleClient!!)


        coordinatorLayout {
            verticalLayout {
                padding = dip(32)
                val onoff = toggleButton{ }.lparams {
                    height = dip(100)
                    width = matchParent

                }

                val brightness = seekBar {
                    min = 0
                    max = 0xFFFF
                }.lparams {
                    width = matchParent
                    bottomMargin = dip(16)
                }

                val pattern = spinner {
                    val choices: Array<String> = Array(255) { i -> i.toString() }

                    //adapter = ArrayAdapter(this@MainActivity, R.layout.text_view, choices)
                    adapter = ArrayAdapter(this@MainActivity, android.R.layout.simple_spinner_dropdown_item, choices)
                }.lparams {
                    width = matchParent
                    bottomMargin = dip(16)
                }

                val color = hslColorPicker { }.lparams {
                    width = matchParent
                    bottomMargin = dip(16)
                }

                buttons.addAll(arrayOf(brightness, color, onoff, pattern))

                onoff.checkedChanges().subscribe({ x -> bt.setOn(x).subscribe({}, {}) }, {})
                brightness.changes().subscribe({ x -> bt.setBrightness(x).subscribe({}, {}) }, {})
//                pattern.itemSelections().subscribe { x -> bt.setPattern(x) }
//                color.colorSelections().subscribe { x -> bt.setColour(x) }

            }
        }

        bt.connState.distinct().subscribe { connected ->
            snackbar?.dismiss()

            if (connected) {
                snackbar = Snackbar.make(this.contentView!!, "Connected!", Snackbar.LENGTH_SHORT)
            } else {
                snackbar = Snackbar.make(this.contentView!!, "Searching...", Snackbar.LENGTH_INDEFINITE)
            }

            snackbar!!.show()

            buttons.forEach { it.isEnabled = connected }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
//            connect()
        }
    }
}
